class Person {
  final String name;
  final String phone;
  final String picture;

  const Person(this.name,this.phone, this.picture);
}

final List<Person> people = _people.map((e) => Person(e['name'].toString(), e['phone'].toString(),e['picture'].toString())).toList(growable: false);

final List<Map<String,Object>> _people = [
  {
    "_id": "frhjsdkfhjhrui34jfdskhwerjkhj",
    "index": 0,
    "guid": "fdsf-fsdfsd-fsdfsd-fsdfds",
    "isActive": true,
    "balance": "\$843995",
    "picture": "http://placehold.it/32x32",
    "age": 38,
    "eyeColor": "green",
    "name": "ahmad ali",
    "gender": "male",
    "company": "some company 1",
    "email": "fdjksl@fkjd.vd",
    "phone": "+1 (434) 4324 4324",
    "address": "fjksdh fjksdfh fkhsdjkf ff sdjkfhshfuayeu fd aueyu",
    "about": "fsdfjkhsdjf",
    "registered": "2018-03-02T03:29:13 +08:00",
    "latitude": -42.5438744,
    "longitude": 155.804324,
    "tags": ["dolor", "nisi", "sunt"],
    "friends":[
      {"id":0, "name":"micheal semon"},
      {"id":1, "name": "ali mond"}
    ],
    "greeting": "hello asshole you have fucking bitch",
    "favoriteFruit": "banana"
  },
  {
    "_id": "frhjsdkfdfhjhrui34jkhwerjkhj",
    "index": 1,
    "guid": "fdsf-fsdfsd-fsdfsd-fsdfds",
    "isActive": true,
    "balance": "\$843995",
    "picture": "http://placehold.it/32x32",
    "age": 38,
    "eyeColor": "green",
    "name": "ahmad ali",
    "gender": "male",
    "company": "some company 1",
    "email": "fdjksl@fkjd.vd",
    "phone": "+1 (434) 3224324 4324",
    "address": "fjksdh fjksdfh fkhsdjkf ff sdjkfhshfuayeu fd aueyu",
    "about": "fsdfjkhsdjf",
    "registered": "2018-03-02T03:29:13 +08:00",
    "latitude": -42.5438744,
    "longitude": 155.804324,
    "tags": ["dolor", "nisi", "sunt"],
    "friends":[
      {"id":0, "name":"micheal semon"},
      {"id":1, "name": "ali mond"}
    ],
    "greeting": "hello asshole you have fucking bitch",
    "favoriteFruit": "banana"
  },{
    "_id": "frhjsdkfhjfhrui34jkhwerjkhj",
    "index": 2,
    "guid": "fdsf-fsdfsd-fsdfsd-fsdfds",
    "isActive": true,
    "balance": "\$843995",
    "picture": "http://placehold.it/32x32",
    "age": 38,
    "eyeColor": "green",
    "name": "ahmad ali",
    "gender": "male",
    "company": "some company 1",
    "email": "fdjksl@fkjd.vd",
    "phone": "+1 (434) 4324 44343324",
    "address": "fjksdh fjksdfh fkhsdjkf ff sdjkfhshfuayeu fd aueyu",
    "about": "fsdfjkhsdjf",
    "registered": "2018-03-02T03:29:13 +08:00",
    "latitude": -42.5438744,
    "longitude": 155.804324,
    "tags": ["dolor", "nisi", "sunt"],
    "friends":[
      {"id":0, "name":"micheal semon"},
      {"id":1, "name": "ali mond"}
    ],
    "greeting": "hello asshole you have fucking bitch",
    "favoriteFruit": "banana"
  },{
    "_id": "frhjsdkfhjhrui34jkrhwerjkhj",
    "index": 3,
    "guid": "fdsf-fsdfsd-fsdfsd-fsdfds",
    "isActive": true,
    "balance": "\$843995",
    "picture": "http://placehold.it/32x32",
    "age": 38,
    "eyeColor": "green",
    "name": "ahmad ali",
    "gender": "male",
    "company": "some company 1",
    "email": "fdjksl@fkjd.vd",
    "phone": "+1 (434) 4324 004324",
    "address": "fjksdh fjksdfh fkhsdjkf ff sdjkfhshfuayeu fd aueyu",
    "about": "fsdfjkhsdjf",
    "registered": "2018-03-02T03:29:13 +08:00",
    "latitude": -42.5438744,
    "longitude": 155.804324,
    "tags": ["dolor", "nisi", "sunt"],
    "friends":[
      {"id":0, "name":"micheal semon"},
      {"id":1, "name": "ali mond"}
    ],
    "greeting": "hello asshole you have fucking bitch",
    "favoriteFruit": "banana"
  },
];