import 'package:adaptive_app/src/people.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          if(constraints.maxWidth > 600) {
            return WideLayout();
          }else {
            return NarrowLayout();
          }

        }
      )// This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class WideLayout extends StatefulWidget {
  @override
  _WideLayoutState createState() => _WideLayoutState();
}

class _WideLayoutState extends State<WideLayout> {
  Person _person = people[1];

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(child: PeopleList(onPersonTap: (person) => setState(() => _person = person)), flex: 2,),
        Expanded(
            child: _person == null ? Placeholder(): PersonDetail(_person),
            flex: 3
        ),
      ],
    );
  }
}


class NarrowLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child:PeopleList(
        onPersonTap: (person) => Navigator.of(context).push(
            MaterialPageRoute(builder: (context) =>
                Scaffold(
                    appBar: AppBar(),
                    body: PersonDetail(person)))),
      )
    );
  }
}

class PeopleList extends StatelessWidget {
  final void Function(Person) onPersonTap;

  const PeopleList({required this.onPersonTap});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        for(var person in people)
          ListTile(
            leading: Image.network(person.picture),
            title: Text(person.name),
            onTap: () => onPersonTap(person)
          )
      ],
    );
  }
}


class PersonDetail extends StatelessWidget {
  final Person person;

  const PersonDetail(this.person);
  @override
  Widget build(BuildContext context) {
    return Center(
             child: Column(
               mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(person.name),
                Text(person.phone)
              ],
            )
          );
  }
}

